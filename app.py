from datetime import datetime
from queue import Queue
from threading import Timer, Thread

import dlib

import numpy as np

import imutils
from apscheduler.schedulers.blocking import BlockingScheduler
from apscheduler.triggers.cron import CronTrigger
from apscheduler.triggers.combining import OrTrigger
from flask import Flask, request, jsonify, abort
from flask_basicauth import BasicAuth
from flask_pymongo import PyMongo
import cv2 as cv2
from imutils.video import VideoStream, FPS
import time
from tzlocal import get_localzone

from pyimagesearch.centroidtracker import CentroidTracker
from pyimagesearch.trackableobject import TrackableObject

app = Flask(__name__)

app.config["MONGO_URI"] = "mongodb://marskcnnserver:marskcnnserveR123456@192.168.1.205:27017/marskcnnserver" \
                          "?authSource=marskcnnserver&authMechanism=SCRAM-SHA-1"
mongo = PyMongo(app)
app.config['BASIC_AUTH_USERNAME'] = 'marskcnnserver'
app.config['BASIC_AUTH_PASSWORD'] = 'marskcnnserveR123456'
# app.config['BASIC_AUTH_FORCE'] = True
basic_auth = BasicAuth(app)

tz = get_localzone()
sched = BlockingScheduler(timezone=tz)
# trigger = OrTrigger([CronTrigger(hour=17, minute=30)]

x = datetime.today()
config = mongo.db.config_count.find({})[0]
secs = config['interval']
camera_add = config['camera_add']
videostream = False
sche = False

with app.app_context():
    def run():
        time_begin = datetime.now()
        CLASSES = ["background", "aeroplane", "bicycle", "bird", "boat",
                   "bottle", "bus", "car", "cat", "chair", "cow", "diningtable",
                   "dog", "horse", "motorbike", "person", "pottedplant", "sheep",
                   "sofa", "train", "tvmonitor"]
        print("[INFO] loading model...")
        net = cv2.dnn.readNetFromCaffe(config['prototxt'], config['model'])
        if not videostream:
            print("[INFO] starting video stream...")
            vs = VideoStream(src=camera_add).start()
            time.sleep(2.0)
        else:
            print("[INFO] opening video file...")
            vs = cv2.VideoCapture(r"D:\MyApp\PRJ\people-counting-opencv\videos\example_01.mp4")
            time.sleep(2.0)
        W = None
        H = None

        ct = CentroidTracker(maxDisappeared=40, maxDistance=50)
        trackers = []
        trackableObjects = {}
        totalFrames = 0
        totalDown = 0
        totalUp = 0
        totalInside = 0
        coordinates = config['coordinates']
        fps = FPS().start()
        while True:
            frame = vs.read()
            if videostream:
                frame = frame[1]
            if frame is None:
                break
            frame = imutils.resize(frame, width=500)
            rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            if W is None or H is None:
                (H, W) = frame.shape[:2]
            status = "Waiting"
            rects = []
            if totalFrames % config['skip_frame'] == 0:
                status = "Detecting"
                trackers = []

                blob = cv2.dnn.blobFromImage(frame, 0.007843, (W, H), 127.5)
                net.setInput(blob)
                detections = net.forward()
                for i in np.arange(0, detections.shape[2]):
                    confidence = detections[0, 0, i, 2]
                    if confidence > config['confidence']:
                        idx = int(detections[0, 0, i, 1])
                        if CLASSES[idx] != "person":
                            continue
                        box = detections[0, 0, i, 3:7] * np.array([W, H, W, H])
                        (startX, startY, endX, endY) = box.astype("int")
                        tracker = dlib.correlation_tracker()
                        rect = dlib.rectangle(startX, startY, endX, endY)
                        tracker.start_track(rgb, rect)

                        trackers.append(tracker)
            else:
                for tracker in trackers:
                    status = "Tracking"

                    # update the tracker and grab the updated position
                    tracker.update(rgb)
                    pos = tracker.get_position()

                    # unpack the position object
                    startX = int(pos.left())
                    startY = int(pos.top())
                    endX = int(pos.right())
                    endY = int(pos.bottom())

                    # add the bounding box coordinates to the rectangles list
                    rects.append((startX, startY, endX, endY))
            door_ = {
                "X": coordinates['x'],
                "Y": coordinates['y'],
                "w": coordinates['w'],
                "h": coordinates['h']
            }
            # "X": 0,
            # "Y": H // 2,
            # "w": W // 2 + 50,
            # "h": H // 3
            # cv2.line(frame, (0, H // 2), (W, H // 2), (0, 255, 255), 1)
            # cv2.line(frame, (0, H // 2 + 1), (W, H // 2 + 1), (0, 0, 255), 1)
            rectangle = cv2.rectangle(frame, (door_['X'], door_['Y']), (door_['X'] + door_['w'], door_['Y'] + door_['h']), (0, 255, 0), 1)
            objects = ct.update(rects)
            for (objectID, centroid) in objects.items():
                to = trackableObjects.get(objectID, None)
                if to is None:
                    to = TrackableObject(objectID, centroid)
                else:
                    x = [c[0] for c in to.centroids]
                    y = [c[1] for c in to.centroids]
                    A = (door_['X'], door_['Y'])  # 0     200 (50)
                    B = (door_['X'] + door_['w'], door_['Y'])
                    C = (door_['X'] + door_['w'], door_['Y'] + door_['h'])  # 100   150
                    D = (door_['X'], door_['Y'] + door_['h'])
                    direction = centroid[1] - np.mean(y)  # toa do y sau - toa dp y truoc
                    # direction < 0 => di len tren
                    # direction > 0 => di xuong duoi
                    to.centroids.append(centroid)

                    if not to.counted:
                        # if A[0] < centroid[0] < C[0] and A[1] < centroid[1] < C[1]:
                        #     totalInside += 1
                        #     to.counted = True
                        # else:
                            if centroid[1] <= A[1] or centroid[1] >= C[1] or centroid[0] <= A[0] or centroid[0] >= C[0]:
                                if direction < 0 and centroid[1] <= door_['Y'] <= np.mean(y):
                                    totalUp += 1
                                    to.counted = True
                                elif direction > 0 and centroid[1] >= (door_['Y'] + door_['h']) >= np.mean(y):
                                    totalDown += 1
                                    to.counted = True

                    # if not to.counted:
                    #     if direction < 0 and centroid[1] < (door_['Y'] + door_['h']):
                    #         totalUp += 1
                    #         to.counted = True
                    #     elif direction > 0 and centroid[1] > (door_['Y'] + door_['h']):
                    #         totalDown += 1
                    #         to.counted = True
                trackableObjects[objectID] = to

                text = "ID {}".format(objectID)
                # centroid[0] = startX
                # centroid[1] = startY
                # centroid[2] = endX
                # centroid[3] = endY
                cv2.putText(frame, text, (centroid[0] - 10, centroid[1] - 10),
                            cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
                cv2.circle(frame, (centroid[0], centroid[1]), 4, (0, 255, 0), -1)
                # cv2.rectangle(frame, (centroid[2], centroid[4]), (centroid[3], centroid[5]), (0, 255, 0), 2)
            info = [
                ("Up", totalUp),
                ("Down", totalDown),
                ("Inside", totalInside),
                ("Status", status),
            ]

            # loop over the info tuples and draw them on our frame
            for (i, (k, v)) in enumerate(info):
                text = "{}: {}".format(k, v)
                cv2.putText(frame, text, (10, H - ((i * 20) + 20)),
                            cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 0, 255), 2)
            cv2.imshow("Frame", frame)
            key = cv2.waitKey(1) & 0xFF

            time_end_frame = datetime.now()

            if key == ord("q"):
                break
            delta = time_end_frame - time_begin
            if not videostream:
                if delta.seconds > secs:
                    break
            totalFrames += 1
            fps.update()
        fps.stop()
        time_end = datetime.now()
        save_db(time_begin, time_end, totalUp, totalDown)
        print("[INFO] elapsed time: {:.2f}".format(fps.elapsed()))
        print("[INFO] approx. FPS: {:.2f}".format(fps.fps()))
        if not videostream:
            vs.stop()
        else:
            vs.release()

        cv2.destroyAllWindows()


    def hello_world(txt):
        # with app.app_context():
        print(txt)
        print("starting...")


    def save_db(time_begin, time_end, up, down):
        post = {
            'time_begin':  time.mktime(time_begin.timetuple()),
            'time_end': time.mktime(time_end.timetuple()),
            'up': up,
            'down': down,
        }
        mongo.db.history_count.insert_one(post)


    # def generate_app():
    #     app = Flask(__name__)
    #     return app

    def handing_thread():
        t1 = Thread(target=run)
        t1.daemon = True
        t1.start()
        t1.join()


    def parrent_func():
        if not videostream:
            if sche:
                begin_time = config['begin_time']
                trigger = OrTrigger([CronTrigger(hour=begin_time['hour'], minute=begin_time['minute'])])
                sched.add_job(handing_thread, trigger)
                # handing_thread()
                try:
                    sched.start()
                except (KeyboardInterrupt, SystemExit):
                    pass
            else:
                handing_thread()
        else:
            handing_thread()

    parrent_func()


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5002)
