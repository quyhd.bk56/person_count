class Config:
    """

    """
    def __init__(self, label, prototxt, model, skip_frame, confidence, camera_add, interval):
        self.label = label
        self.prototxt = prototxt
        self.model = model
        self.skip_frame = skip_frame
        self.confidence = confidence
        self.camera_add = camera_add
        self.interval = interval

        # "_id": "b5d84330-21fa-4c7b-80af-4335c49cc969",
        # "label": "person",
        # "prototxt": "mobinet ssd/MobileNetSSD_deploy.prototxt",
        # "model": "mobinet ssd/MobileNetSSD_deploy.caffemodel",
        # "skip_frame": 30,
        # "confidence": 0.4,
        # "camera_add": "rtsp://admin:admin0987888620@192.168.1.111:554/cam/realmonitor?channel=1&subtype=0"
